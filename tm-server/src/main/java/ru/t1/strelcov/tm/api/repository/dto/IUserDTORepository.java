package ru.t1.strelcov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.UserDTO;

public interface IUserDTORepository extends IDTORepository<UserDTO> {

    @Nullable
    UserDTO findFirstByLogin(@NotNull String login);

    void deleteByLogin(@NotNull String login);

    boolean existsByLogin(@NotNull final String login);

}
