package ru.t1.strelcov.tm.configuration;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.t1.strelcov.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.util.Properties;

@EnableJpaRepositories("ru.t1.strelcov.tm.api.repository")
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@ComponentScan("ru.t1.strelcov.tm")
public class ServerConfiguration {

    @SneakyThrows
    @Bean(initMethod = "start", destroyMethod = "stop")
    public BrokerService broker(@NotNull final IPropertyService propertyService) {
        BasicConfigurator.configure();
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector(propertyService.getMQConnectionFactory());
        return broker;
    }

    @Bean
    @NotNull
    public DataSource dataSource(
            @NotNull @Value("${jdbc.driver}") final String databaseDriver,
            @NotNull @Value("${jdbc.url}") final String databaseUrl,
            @NotNull @Value("${jdbc.user}") final String databaseUserName,
            @NotNull @Value("${jdbc.password}") final String databaseUserPassword
    ) {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseDriver);
        dataSource.setUrl(databaseUrl);
        dataSource.setUsername(databaseUserName);
        dataSource.setPassword(databaseUserPassword);
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull final DataSource dataSource,
            @NotNull @Value("${database.dialect}") final String databaseDialect,
            @NotNull @Value("${database.hbm2ddl_auto}") final String databaseHbm2ddlAuto,
            @NotNull @Value("${database.show_sql}") final String databaseShowSql,
            @NotNull @Value("${database.format_sql}") final String databaseFormatSql,
            @NotNull @Value("${database.cache.use_second_level_cache}") final String databaseUseL2Cache,
            @NotNull @Value("${database.cache.provider_configuration_file_resource_path}") final String databaseProviderConfigFileResourcePath,
            @NotNull @Value("${database.cache.region.factory_class}") final String databaseRegionFactoryClass,
            @NotNull @Value("${database.cache.user_query_cache}") final String databaseUserQueryCache,
            @NotNull @Value("${database.cache.use_minimal_puts}") final String databaseUseMinimalPuts,
            @NotNull @Value("${database.cache.hazelcast.use_lite_member}") final String databaseUseLiteMember,
            @NotNull @Value("${database.cache.region_prefix}") final String databaseRegionPrefix
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.strelcov.tm.dto.model", "ru.t1.strelcov.tm.model");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, databaseDialect);
        properties.put(Environment.HBM2DDL_AUTO, databaseHbm2ddlAuto);
        properties.put(Environment.SHOW_SQL, databaseShowSql);
        properties.put(Environment.FORMAT_SQL, databaseFormatSql);
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseUseL2Cache);
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProviderConfigFileResourcePath);
        properties.put(Environment.CACHE_REGION_FACTORY, databaseRegionFactoryClass);
        properties.put(Environment.USE_QUERY_CACHE, databaseUserQueryCache);
        properties.put(Environment.USE_MINIMAL_PUTS, databaseUseMinimalPuts);
        properties.put("hibernate.cache.hazelcast.use_lite_member", databaseUseLiteMember);
        properties.put(Environment.CACHE_REGION_PREFIX, databaseRegionPrefix);
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

//Initialization with persistence.xml
/*    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NotNull @Value("${persistence.unit}") final String persistenceUnitName
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPersistenceUnitName(persistenceUnitName);
        factoryBean.setPersistenceXmlLocation("classpath:META-INF/persistence.xml");
        return factoryBean;
    }*/

    @Bean
    @NotNull
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
