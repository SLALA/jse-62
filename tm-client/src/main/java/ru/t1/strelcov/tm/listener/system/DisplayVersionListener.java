package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

@Component
public final class DisplayVersionListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-v";
    }

    @NotNull
    @Override
    public String name() {
        return "version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display program version.";
    }

    @Override
    @EventListener(condition = "@displayVersionListener.name() == #event.name || @displayVersionListener.arg() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[VERSION]");
        System.out.println(propertyService.getVersion());
    }

}
